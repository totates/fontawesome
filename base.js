import { dom } from "external:@totates/fontawesome/v1:upstream.script";

export function i2svg(node) {
    /* as of 5.7.0, returns a promise */
    return dom.i2svg({ node });
    // return new Promise(resolve => dom.i2svg({ node, callback: resolve }));
}
