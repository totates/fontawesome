import logging from "external:@totates/logging/v1";
import { QuotquotElement } from "external:@totates/quotquot/v1:element";
import StylePlugin from "external:@totates/quotquot/v1:plugin.style";
import { i2svg } from "external:@totates/fontawesome/v1:base";

import { tagName } from "./index.json";
import resources from "./resources.js";

const logger = logging.getLogger(`@totates/maplibre-gl/v1:element.${tagName}`);

const styles = [resources["@totates/fontawesome/v1"]["upstream.style"]];

class FontawesomeIconHTMLElement extends QuotquotElement {
    constructor() {
        const state = { styles };
        super({ state, Plugins: [StylePlugin] });
        this._init().catch(logger.error);
    }

    async _init() {
        const i = document.createElement("i");
        i.className = `fas fa-${this.icon}`;

        const size = this.getAttribute("size");
        if (size) {
            if (["lg", "2x", "3x", "4x", "5x"].indexOf(size) !== -1)
                i.classList.add(`fa-${size}`);
            else
                logger.warn(`unknown icon size '${size}'`);
        }

        const span = document.createElement("span");
        span.appendChild(i);

        this.shadowRoot.appendChild(span);
        await i2svg(span);
    }

    get icon() {
        return this.getAttribute("icon");
    }
}

/* https://stackoverflow.com/questions/49373040/ */
window.customElements.define(tagName, FontawesomeIconHTMLElement);
