import FontawesomeIconHTMLElement from "external:@totates/fontawesome/v1:element.fontawesome-icon.index";

import { tagName } from "./index.json";

class FontawesomeBackElement extends FontawesomeIconHTMLElement {
    constructor() {
        super();
        this.addEventListener("click", e => window.history.back());
    }

    get icon() {
        return "chevron-left"; // "arrow-left";
    }
}

window.customElements.define(tagName, FontawesomeBackElement);
