import { config, library } from "external:@totates/fontawesome/v1:upstream.script";

config.autoAddCss = false;

const loaded = new Set();

export function add(...iconNames) {
    const promises = [];
    for (const iconName of iconNames) {
        if (!loaded.has(iconName)) {
            // TODO: build url from import.meta.url
            const url = `icons/${iconName}.json`;
            promises.push(window.fetch(url).then(json => library.add(json)));
        }
    }
    return Promise.all(promises);
}
