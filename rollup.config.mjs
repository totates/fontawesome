import fs from "fs-extra";
import path from "path";
import resolve from "@rollup/plugin-node-resolve";
import terser from "@rollup/plugin-terser";
import { Component } from "totates";
import { fas } from "@fortawesome/free-solid-svg-icons";

import data from "./component.json" assert { type: "json" };
const component = new Component(data);

async function buildStart(options) {

    const coreDir = "node_modules/@fortawesome/fontawesome-svg-core/";
    await component.manifest.processResourceFile(
        "upstream.script",
        path.join(coreDir, "index.mjs"),
        "application/javascript",
        component.destDir,
        "fontawesome-svg-core.js"
    );
    await component.manifest.processResourceFile(
        "upstream.style",
        path.join(coreDir, "styles.css"),
        "text/css",
        component.destDir,
        "styles.css"
    );

    const iconsDir = path.join(component.destDir, "icons");
    await fs.ensureDir(iconsDir);
    for (const icon of Object.values(fas)) {
        await component.manifest.addResourceFromContent(
            `icon.${icon.iconName}`,
            JSON.stringify(icon),
            "application/json",
            path.join(component.destDir, "icons"),
            `${icon.iconName}.json`
        );
    }
}

export default [
    {
        input: "base.js",
        output: {
            file: path.join(component.destDir, "base.js"),
            format: "esm"
        },
        makeAbsoluteExternalsRelative: false,
        plugins: [
            {
                name: "fontawesome",
                buildStart
            },
            resolve({ browser: true }),
            component.resolver(),
            component.resource("base", "application/javascript"),
            process.env.BUILD === "production" && terser()
        ]
    },
    ...component.getConfig()
];
